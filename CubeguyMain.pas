unit CubeguyMain;

{$mode objfpc}{$H+}

interface

uses
  GR32, GR32_Image,
  LCLType,
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  CubeguyLevel, GR32_Layers;

const
  VERSION_TEXT = 'V1.05';

  FPS = 15;
  FRAME_DELAY = 1000 div FPS;

  IMG_BRICK = 0;
  IMG_BLOCK = 1;
  IMG_GOAL = 2;
  IMG_GUY_RIGHT = 3;
  IMG_GUY_RIGHT_HOLD = 4;
  IMG_GUY_LEFT = 5;
  IMG_GUY_LEFT_HOLD = 6;
  IMG_GUY_WIN = 7;
  IMG_EDIT_TILE = 8;

  SFX_FALL = 1;
  SFX_MOVE = 2;
  SFX_CLIMB = 3;
  SFX_PICK = 4;
  SFX_DROP = 5;
  SFX_WIN = 6;
  SFX_FAIL = 7;

  SPRITE_WIDTH = 48;
  SPRITE_HEIGHT = 40;
  SPRITE_HOTSPOT_X = 0;
  SPRITE_HOTSPOT_Y = 8;

  SCALE_COUNT = 5;
  DEFAULT_SCALE = 2;
  GFX_SCALE: array[0..SCALE_COUNT-1] of Double = (0.25, 0.5, 1, 2, 4);

type

  { TCubeguyForm }

  TCubeguyForm = class(TForm)
    imgMain: TImage32;
    MainMenu1: TMainMenu;
    miZoomOut: TMenuItem;
    miZoomDefault: TMenuItem;
    mi3DView: TMenuItem;
    miViewDivider: TMenuItem;
    msView: TMenuItem;
    miZoomIn: TMenuItem;
    miHelpDivider: TMenuItem;
    miHelpAbout: TMenuItem;
    miHelpEditMode: TMenuItem;
    miHelpGame: TMenuItem;
    msHelp: TMenuItem;
    miFileEditMode: TMenuItem;
    miFileDivider1: TMenuItem;
    msFile: TMenuItem;
    miReplaySave: TMenuItem;
    miReplayRestart: TMenuItem;
    miReplayUndo: TMenuItem;
    miFileImport: TMenuItem;
    miFileNew: TMenuItem;
    miFileOpen: TMenuItem;
    miFileSave: TMenuItem;
    miFileSaveAs: TMenuItem;
    miFileDivider2: TMenuItem;
    miFileExit: TMenuItem;
    msReplay: TMenuItem;
    miReplayOpen: TMenuItem;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);

    procedure GameUpdate(Sender: TObject; var Done: Boolean);
    procedure imgMainMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer; Layer: TCustomLayer);
    procedure mi3DViewClick(Sender: TObject);
    procedure miFileEditModeClick(Sender: TObject);
    procedure miHelpAboutClick(Sender: TObject);
    procedure miHelpEditModeClick(Sender: TObject);
    procedure miHelpGameClick(Sender: TObject);
    procedure miReplaySaveClick(Sender: TObject);
    procedure miReplayRestartClick(Sender: TObject);
    procedure miReplayUndoClick(Sender: TObject);
    procedure miFileImportClick(Sender: TObject);
    procedure miFileNewClick(Sender: TObject);
    procedure miFileOpenClick(Sender: TObject);
    procedure miFileSaveClick(Sender: TObject);
    procedure miFileSaveAsClick(Sender: TObject);
    procedure miFileExitClick(Sender: TObject);
    procedure miReplayOpenClick(Sender: TObject);
    procedure miZoomDefaultClick(Sender: TObject);
    procedure miZoomInClick(Sender: TObject);
    procedure miZoomOutClick(Sender: TObject);
  private
    fLevelFilename: String;
    fLevelSaved: Boolean;
    fNeedRender: Boolean;
    fGraphics: TBitmap32;
    fBuffer: TBitmap32;
    fState: TCubeguyLevelState;
    fLevel: TCubeguyLevel;
    fScroll: TPoint;
    fScrollMode: Boolean;
    fLastUpdate: Int64;
    fReplay: String;
    fReplayState: Integer;
    fReplayInstant: Boolean;
    fScaleIndex: Integer;

    procedure LoadGraphics;

    function CanMoveForward: Boolean;
    function CanMoveUp: Boolean;
    function CanPickUp: Boolean;
    function CanPutDown: Boolean;
    function HandleUnsupportedBlocks: Boolean;
    function HandleUnsupportedGuy: Boolean;

    function GetFrameRect(const ImgIndex: Integer): TRect;

    procedure Render;
    procedure CopyBufferToScreen;

    procedure PlaySound(const SndIndex: Integer);

    procedure HandleInput;

    procedure MakeDefaultMap;
    function DoSaveLevel: Boolean;

    function GetMouseTile: TPoint;

    procedure HandleEditModeKey(Key: Word; Shift: TShiftState);

    function CheckSave: Boolean;
    procedure DoImport(Filename: String);
  public

  end;

var
  CubeguyForm: TCubeguyForm;

implementation

{$R *.lfm}

{ TCubeguyForm }

// Initialization

procedure TCubeguyForm.FormCreate(Sender: TObject);
begin
  fGraphics := TBitmap32.Create;
  fBuffer := TBitmap32.Create;
  fLevel := TCubeguyLevel.Create;
  fState := TCubeguyLevelState.Create;
  fNeedRender := true;
  fReplayState := 0;
  fReplay := '';
  LoadGraphics;

  MakeDefaultMap; // debug

  Application.OnIdle := @GameUpdate;
  fLastUpdate := GetTickCount64;

  fScaleIndex := DEFAULT_SCALE;
  imgMain.Scale := GFX_SCALE[fScaleIndex];
end;

procedure TCubeguyForm.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  CanClose := CheckSave;
end;

procedure TCubeguyForm.FormDestroy(Sender: TObject);
begin
  fGraphics.Free;
  fBuffer.Free;
  fLevel.Free;
  fState.Free;
end;

procedure TCubeguyForm.MakeDefaultMap;
var
  x: Integer;
begin
  fLevel.Clear;

  fLevel.SetSize(8, 8);
  for x := 0 to 7 do
  begin
    fLevel[x, 0] := ctBrick;
    fLevel[x, 7] := ctBrick;
    fLevel[0, x] := ctBrick;
    fLevel[7, x] := ctBrick;
  end;

  fLevel[5, 6] := ctBrick;
  fLevel[6, 6] := ctBrick;
  fLevel[6, 5] := ctBrick;

  fLevel[1, 6] := ctBlock;
  fLevel[2, 6] := ctBlock;
  fLevel[1, 5] := ctBlock;

  fLevel[1, 2] := ctBrick;

  fLevel.Start := Point(1, 1);
  fLevel.StartDirection := cdRight;
  fLevel.Goal := Point(6, 4);

  fState.Assign(fLevel);
  fReplayState := 0;
  fReplay := '';
  fReplayInstant := false;

  fNeedRender := true;
  fLevelSaved := true;
end;

procedure TCubeguyForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if miFileEditMode.Checked then
    HandleEditModeKey(Key, Shift);

  if fReplayInstant then Exit;

  if fReplayState > 0 then
  begin
    fReplay := LeftStr(fReplay, fReplayState-1);
    fReplayState := 0;
  end;

  if (Key = VK_LEFT) or (Key = VK_NUMPAD4) then
    fState.Action := caLeft;
  if (Key = VK_RIGHT) or (Key = VK_NUMPAD6) then
    fState.Action := caRight;
  if (Key = VK_UP) or (Key = VK_NUMPAD8) then
    fState.Action := caUp;
  if (Key = VK_DOWN) or (Key = VK_NUMPAD2) then
    fState.Action := caPick;

  fScrollMode := (ssCtrl in Shift) or (ssMeta in Shift); // Mac friendly, just in case
end;

procedure TCubeguyForm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if miFileEditMode.Checked then Exit;

  if ((Key = VK_LEFT) or (Key = VK_NUMPAD4)) and (fState.Action = caLeft) then
    fState.Action := caNone;
  if ((Key = VK_RIGHT) or (Key = VK_NUMPAD6)) and (fState.Action = caRight) then
    fState.Action := caNone;
  if ((Key = VK_UP) or (Key = VK_NUMPAD8)) and (fState.Action = caUp) then
    fState.Action := caNone;
  if ((Key = VK_DOWN) or (Key = VK_NUMPAD2)) and (fState.Action = caPick) then
    fState.Action := caNone;
end;

procedure TCubeguyForm.FormResize(Sender: TObject);
begin
  fNeedRender := true;
end;

procedure TCubeguyForm.LoadGraphics;
var
  x, y: Integer;
begin
  fGraphics.LoadFromFile('gfx.bmp');
  fGraphics.DrawMode := dmBlend;
  fGraphics.CombineMode := cmBlend;
  for y := 0 to fGraphics.Height-1 do
    for x := 0 to fGraphics.Width-1 do
      if (fGraphics[x, y] and $FFFFFF) = $FF00FF then
        fGraphics[x, y] := 0;
end;

// Update cycle

procedure TCubeguyForm.GameUpdate(Sender: TObject; var Done: Boolean);
var
  TcNow: Int64;
begin
  try
    Done := false;

    TcNow := GetTickCount64;
    if (TcNow - fLastUpdate < FRAME_DELAY) and not fReplayInstant then Exit;
    fLastUpdate := TcNow;

    if not (fNeedRender or miFileEditMode.Checked) then
    begin
      if HandleUnsupportedBlocks then
        fNeedRender := true;

      if HandleUnsupportedGuy then
        fNeedRender := true;
    end;

    if fNeedRender then
    begin
      if not fReplayInstant then
        Render;
      fNeedRender := false;
      Exit;
    end;

    if miFileEditMode.Checked then Exit;

    if fReplayState > 0 then
    begin
      if fReplayState > Length(fReplay) then
      begin
        fState.Action := caNone;
        fReplayState := 0;
        if fReplayInstant then
          fNeedRender := true;
        fReplayInstant := false;
        Exit;
      end;

      case fReplay[fReplayState] of
        '<': fState.Action := caLeft;
        '>': fState.Action := caRight;
        '^': fState.Action := caUp;
        'v': fState.Action := caPick;
      end;
      Inc(fReplayState);
    end;
    HandleInput;

  finally
    if not fReplayInstant then Sleep(1); // relax CPU
  end;
end;

procedure TCubeguyForm.imgMainMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer; Layer: TCustomLayer);
begin
  if miFileEditMode.Checked then
    fNeedRender := true;
end;

procedure TCubeguyForm.mi3DViewClick(Sender: TObject);
begin
  mi3DView.Checked := not mi3DView.Checked;
  fNeedRender := true;
end;

procedure TCubeguyForm.miFileEditModeClick(Sender: TObject);
begin
  miFileEditMode.Checked := not miFileEditMode.Checked;
  if miFileEditMode.Checked then
  begin
    fState.Assign(fLevel);
    fReplay := '';
    fReplayState := 0;
    fReplayInstant := false;
    fNeedRender := true;
  end;
end;

procedure TCubeguyForm.miHelpAboutClick(Sender: TObject);
begin
  ShowMessage('Cubeguy ' + VERSION_TEXT + ' by Namida Verasche' + #10 +
              'Thanks to developers of Graphics32, Lazarus Component Library and FreePascal Runtime Library' + #10 +
              'All graphical resources and all other code by Namida Verasche' + #10 +
              #10 +
              'Cubeguy is distributed free of charge and if you paid for it you are a dumbass who got ripped off' + #10 +
              'Cubeguy is a shameless ripoff of Block-Man 1, which is in turn a shameless ripoff of Blockdude');
end;

procedure TCubeguyForm.miHelpEditModeClick(Sender: TObject);
begin
  ShowMessage('Edit mode controls:' + #10 +
              #10 +
              'Use mouse to select target location' + #10 +
              'Use arrow keys or numpad to scroll' + #10 +
              'Use Ctrl+arrow / numpad to add extra row / column on a side' + #10 +
              'Use Alt+arrow / numpad to delete row on a side' + #10 +
              #10 +
              'Use following keys to place an item:' + #10 +
              'Z: Empty space' + #10 +
              'X: Bricks' + #10 +
              'C: Moveable block' + #10 +
              'V: Goal' + #10 +
              'B: Start (facing left)' + #10 +
              'N: Start (facing right)');
end;

procedure TCubeguyForm.miHelpGameClick(Sender: TObject);
begin
  ShowMessage('Your goal is to get to the flag. Your moves:' + #10 +
              #10 +
              'Left Arrow or Numpad 4: Face left, and if possible, move forward' + #10 +
              'Right Arrow or Numpad 6: Face right, and if possible, move forward' + #10 +
              'Up Arrow or Numpad 8: Climb up one block if possible' + #10 +
              'Down Arrow or Numpad 2: Pick up or put down moveable block' + #10 +
              #10 +
              'You can only pick up a block immediately in front of you with nothing on it.' + #10 +
              'You can put down a block immediately in front of you at your level or one block higher.');
end;

procedure TCubeguyForm.miReplaySaveClick(Sender: TObject);
var
  SaveDlg: TSaveDialog;
  SL: TStringList;
begin
  SaveDlg := TSaveDialog.Create(self);
  SL := TStringList.Create; // lazy
  try
    SaveDlg.Filter := 'Cubeguy Replay (*.crp)|*.crp';
    SaveDlg.Options := [ofOverwritePrompt];
    if not SaveDlg.Execute then Exit;

    SL.Add(fReplay);
    SL.SaveToFile(SaveDlg.FileName);
  finally
    SaveDlg.Free;
    SL.Free;
  end;
end;

procedure TCubeguyForm.miReplayRestartClick(Sender: TObject);
begin
  fReplay := '';
  fReplayState := 0;
  fReplayInstant := false;
  fState.Assign(fLevel);
  fNeedRender := true;
end;

procedure TCubeguyForm.miReplayUndoClick(Sender: TObject);
begin
  fReplay := LeftStr(fReplay, Length(fReplay)-1);
  fReplayState := 1;
  fReplayInstant := true;
  fState.Assign(fLevel);
end;

procedure TCubeguyForm.miFileImportClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
begin
  if not CheckSave then Exit;

  OpenDlg := TOpenDialog.Create(self);
  try
    OpenDlg.Filter := 'Block-Man 1 Level Screenshot from DOSBox (*.bmp)|*.bmp';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    if not OpenDlg.Execute then Exit;
    DoImport(OpenDlg.Filename);
    fLevelFilename := '';
  finally
    OpenDlg.Free;
  end;
end;

procedure TCubeguyForm.DoImport(Filename: String);
var
  BMP: TBitmap32;
  x, y: Integer;
  Range: TRect;

  function GetTileKeyColor(aX, aY: Integer): Integer;
  var
    C: TColor32;
  begin
    C := BMP[(aX * 20) + 8, (aY * 18) + 13 + 3];
    Result := ((C and $F0) shr 4) +
              ((C and $F000) shr 8) +
              ((C and $F00000) shr 12);
  end;

begin
  BMP := TBitmap32.Create;
  try
    BMP.LoadFromFile(FileName);
    Range := Rect(18, 27, -1, -1);
    for y := 0 to 18 do
      for x := 0 to 27 do
        if (GetTileKeyColor(x, y) <> $000) and (GetTileKeyColor(x, y) <> $555) then
        begin
          if x < Range.Left then Range.Left := x;
          if x + 1 > Range.Right then Range.Right := x + 1;
          if y < Range.Top then Range.Top := y;
          if y + 1 > Range.Bottom then Range.Bottom := y + 1;
        end;

    fLevel.SetSize(Range.Width, Range.Height);

    for y := Range.Top to Range.Bottom-1 do
      for x := Range.Left to Range.Right-1 do
      begin
        case GetTileKeyColor(x, y) of
          $55F: begin fLevel.Start := Point(x - Range.Left, y - Range.Top); fLevel.StartDirection := cdLeft; fLevel[x - Range.Left, y - Range.Top] := ctEmpty; end;
          $FFF: begin fLevel.Start := Point(x - Range.Left, y - Range.Top); fLevel.StartDirection := cdRight; fLevel[x - Range.Left, y - Range.Top] := ctEmpty; end;
          $5F5: begin fLevel.Goal := Point(x - Range.Left, y-Range.Top); fLevel[x - Range.Left, y - Range.Top] := ctEmpty; end;
          $A00: fLevel[x - Range.Left, y - Range.Top] := ctBrick;
          $00A: fLevel[x - Range.Left, y - Range.Top] := ctBlock;
          else fLevel[x - Range.Left, y - Range.Top] := ctEmpty;
        end;
      end;

    fReplay := '';
    fReplayState := 0;
    fState.Assign(fLevel);
    fScroll := fState.Position;
    fNeedRender := true;
    fLevelFilename := '';
    fLevelSaved := false;
    miFileEditMode.Checked := false;
  finally
    BMP.Free;
  end;
end;

procedure TCubeguyForm.miFileNewClick(Sender: TObject);
begin
  if not CheckSave then Exit;

  MakeDefaultMap;
  miFileEditMode.Checked := true;
  fLevelFilename := '';
end;

procedure TCubeguyForm.miFileOpenClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
begin
  if not CheckSave then Exit;

  OpenDlg := TOpenDialog.Create(self);
  try
    OpenDlg.Filter := 'Cubeguy Level (*.clv)|*.clv';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    if not OpenDlg.Execute then Exit;

    fLevelFilename := OpenDlg.Filename;
    fLevelSaved := true;
    fLevel.LoadFromFile(OpenDlg.Filename);

    fReplayState := 0;
    fReplay := '';
    fReplayInstant := false;
    fNeedRender := true;
    fState.Assign(fLevel);
    fScroll := fState.Position;
    miFileEditMode.Checked := false;
  finally
    OpenDlg.Free;
  end;
end;

procedure TCubeguyForm.miFileSaveClick(Sender: TObject);
begin
  DoSaveLevel;
end;

procedure TCubeguyForm.miFileSaveAsClick(Sender: TObject);
var
  OldName: String;
begin
  OldName := fLevelFilename;
  fLevelFilename := '';
  if not DoSaveLevel then
    fLevelFilename := OldName;
end;

function TCubeguyForm.DoSaveLevel: Boolean;
var
  SaveDlg: TSaveDialog;
begin
  Result := false;
  if fLevelFilename = '' then
  begin
    SaveDlg := TSaveDialog.Create(self);
    try
      SaveDlg.Filter := 'Cubeguy Level (*.clv)|*.clv';
      SaveDlg.Options := [ofOverwritePrompt];
      if not SaveDlg.Execute then Exit;
      fLevelFilename := SaveDlg.FileName;
    finally
      SaveDlg.Free;
    end;
  end;

  fLevel.SaveToFile(fLevelFilename);
  fLevelSaved := true;
  Result := true;
end;

procedure TCubeguyForm.miFileExitClick(Sender: TObject);
begin
  Close;
end;

procedure TCubeguyForm.miReplayOpenClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  SL: TStringList; // lazy again
  i: Integer; // but at least not too lazy to handle multi-line
begin
  OpenDlg := TOpenDialog.Create(self);
  SL := TStringList.Create;
  try
    OpenDlg.Filter := 'Cubeguy Replay (*.crp)|*.crp';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    if not OpenDlg.Execute then Exit;

    SL.LoadFromFile(OpenDlg.FileName);

    fReplay := '';
    for i := 0 to SL.Count-1 do
      fReplay := fReplay + SL[i];

    fReplayState := 1;
    fReplayInstant := false;
    fState.Assign(fLevel);
    fNeedRender := true;
  finally
    OpenDlg.Free;
    SL.Free;
  end;
end;

procedure TCubeguyForm.miZoomDefaultClick(Sender: TObject);
begin
  if fScaleIndex = DEFAULT_SCALE then Exit;
  fScaleIndex := DEFAULT_SCALE;
  imgMain.Scale := GFX_SCALE[fScaleIndex];
  fNeedRender := true;
end;

procedure TCubeguyForm.miZoomInClick(Sender: TObject);
begin
  if fScaleIndex = SCALE_COUNT-1 then Exit;
  Inc(fScaleIndex);
  imgMain.Scale := GFX_SCALE[fScaleIndex];
  fNeedRender := true;
end;

procedure TCubeguyForm.miZoomOutClick(Sender: TObject);
begin
  if fScaleIndex = 0 then Exit;
  Dec(fScaleIndex);
  imgMain.Scale := GFX_SCALE[fScaleIndex];
  fNeedRender := true;
end;

procedure TCubeguyForm.HandleInput;
var
  Moved: Boolean;
begin
  if fState.Action = caNone then Exit;

  if fScrollMode then
  begin
    if fState.Action = caLeft then fScroll.X := fScroll.X - 1;
    if fState.Action = caRight then fScroll.X := fScroll.X + 1;
    if fState.Action = caUp then fScroll.Y := fScroll.Y - 1;
    if fState.Action = caPick then fScroll.Y := fScroll.Y + 1;
    fNeedRender := true;
    Exit;
  end;

  if fState.Position = fState.Goal then Exit;
  Moved := false;

  if fState.Action in [caLeft, caRight] then
  begin
    if (fState.Action = caLeft) and (fState.Direction = cdRight) then
    begin
      fState.Direction := cdLeft;
      Moved := true;
    end else if (fState.Action = caRight) and (fState.Direction = cdLeft) then
    begin
      fState.Direction := cdRight;
      Moved := true;
    end;

    if CanMoveForward then
    begin
      fState.Position := Point(fState.Position.X + fState.DX, fState.Position.Y);
      PlaySound(SFX_MOVE);
      Moved := true;

      if fState.HoldingBlock and (fState[fState.Position.X, fState.Position.Y - 1] <> ctEmpty) then
      begin
        fState.HoldingBlock := false;
        fState[fState.Position.X - fState.DX, fState.Position.Y - 1] := ctBlockGravity;
      end;
    end else
      PlaySound(SFX_FAIL);

    if Moved then
    begin
      fNeedRender := true;
      if fReplayState = 0 then
        if fState.Action = caLeft then
          fReplay := fReplay + '<'
        else
          fReplay := fReplay + '>';
    end;
  end;

  if fState.Action = caUp then
  begin
    if CanMoveUp then
    begin
      fNeedRender := true;

      fState.Position := Point(fState.Position.X + fState.DX, fState.Position.Y - 1);
      PlaySound(SFX_CLIMB);
      Moved := true;

      if fState.HoldingBlock and (fState[fState.Position.X, fState.Position.Y - 1] <> ctEmpty) then
      begin
        fState.HoldingBlock := false;
        fState[fState.Position.X - fState.DX, fState.Position.Y - 1] := ctBlockGravity;
      end;

      if fReplayState = 0 then
        fReplay := fReplay + '^';
    end else
      PlaySound(SFX_FAIL);
  end;

  if Moved then
  begin
    fScroll := fState.Position;
    if (fState.Position = fState.Goal) then
      PlaySound(SFX_WIN);
  end;

  if fState.Action = caPick then
  begin
    if fState.HoldingBlock then
    begin
      // holding block
      if CanPutDown then
      begin
        fNeedRender := true;
        fState.HoldingBlock := false;
        fState[fState.Position.X + fState.DX, fState.Position.Y - 1] := ctBlockGravity;
        PlaySound(SFX_DROP);
        if fReplayState = 0 then
          fReplay := fReplay + 'v';
      end else
        PlaySound(SFX_FAIL);
    end else begin
      if CanPickUp then
      begin
        fNeedRender := true;
        fState.HoldingBlock := true;
        fState[fState.Position.X + fState.DX, fState.Position.Y] := ctEmpty;
        PlaySound(SFX_PICK);
        if fReplayState = 0 then
          fReplay := fReplay + 'v';
      end else
        PlaySound(SFX_FAIL);
    end;

    fState.Action := caNone; // this one shouldn't repeat like the others do
  end;
end;

// Move check

function TCubeguyForm.CanMoveForward: Boolean;
begin
  Result := fState[fState.Position.X + fState.DX, fState.Position.Y] = ctEmpty;
end;

function TCubeguyForm.CanMoveUp: Boolean;
begin
  Result := (fState[fState.Position.X + fState.DX, fState.Position.Y] <> ctEmpty) and
            (fState[fState.Position.X + fState.DX, fState.Position.Y - 1] = ctEmpty);

  if fState.HoldingBlock then
    Result := Result and (fState[fState.Position.X, fState.Position.Y-2] = ctEmpty);
end;

function TCubeguyForm.CanPickUp: Boolean;
begin
  Result := (not fState.HoldingBlock) and
            (fState[fState.Position.X, fState.Position.Y - 1] = ctEmpty) and
            (fState[fState.Position.X + fState.DX, fState.Position.Y - 1] = ctEmpty) and
            (fState[fState.Position.X + fState.DX, fState.Position.Y] in [ctBlock, ctBlockGravity]);
end;

function TCubeguyForm.CanPutDown: Boolean;
begin
  Result := (fState.HoldingBlock) and
            (fState[fState.Position.X + fState.DX, fState.Position.Y - 1] = ctEmpty);
  // don't need to check +DX,+0
end;

// Falling

function TCubeguyForm.HandleUnsupportedBlocks: Boolean;
var
  x, y: Integer;
begin
  Result := false;
  for y := fState.Height-1 downto 0 do
    for x := 0 to fState.Width-1 do
      if (fState[x, y] = ctBlockGravity) and (fState[x, y+1] = ctEmpty) then
      begin
        Result := true;
        fState[x, y] := ctEmpty;
        fState[x, y+1] := ctBlockGravity;
      end;
end;

function TCubeguyForm.HandleUnsupportedGuy: Boolean;
begin
  if fState[fState.Position.X, fState.Position.Y + 1] = ctEmpty then
  begin
    fState.Position := Point(fState.Position.X, fState.Position.Y + 1);
    fScroll := fState.Position;
    Result := true;
    PlaySound(SFX_FALL);
  end else
    Result := false;
end;

// Rendering

procedure TCubeguyForm.Render;
var
  x, y: Integer;
  DrawX, DrawY: Integer;
  CharImg: Integer;
  MouseTile: TPoint;
begin
  fBuffer.BeginUpdate;
  try
    if miFileEditMode.Checked then
      fBuffer.Clear($FFC000C0)
    else
      fBuffer.Clear($FF000000);

    if fState.Position = fState.Goal then
      CharImg := IMG_GUY_WIN
    else begin
      if fState.Direction = cdLeft then
        CharImg := IMG_GUY_LEFT
      else
        CharImg := IMG_GUY_RIGHT;
      if fState.HoldingBlock then
        Inc(CharImg);
    end;

    if miFileEditMode.Checked then
      MouseTile := GetMouseTile
    else
      MouseTile := Point(-1, -1);

    if (fBuffer.Width <> fState.Width * 32) or (fBuffer.Height <> fState.Height * 32) then
      fBuffer.SetSize(fState.Width * 32, fState.Height * 32);
    for y := fState.Height-1 downto 0 do
    begin
      DrawY := y * 32 - SPRITE_HOTSPOT_Y;
      for x := 0 to fState.Width-1 do
      begin
        DrawX := x * 32 - SPRITE_HOTSPOT_X;
        case fState[x, y] of
          ctBrick: fBuffer.Draw(DrawX, DrawY, GetFrameRect(IMG_BRICK), fGraphics);
          ctBlock, ctBlockGravity: fBuffer.Draw(DrawX, DrawY, GetFrameRect(IMG_BLOCK), fGraphics);
        end;

        if (x = fState.Goal.X) and (y = fState.Goal.Y) then
          fBuffer.Draw(DrawX, DrawY, GetFrameRect(IMG_GOAL), fGraphics);

        if fState.HoldingBlock and
           (x = fState.Position.X) and (y = fState.Position.Y - 1) then
          fBuffer.Draw(DrawX, DrawY, GetFrameRect(IMG_BLOCK), fGraphics);

        if (x = fState.Position.X) and (y = fState.Position.Y) then
          fBuffer.Draw(DrawX, DrawY, GetFrameRect(CharImg), fGraphics);

        if (x = MouseTile.X) and (y = MouseTile.Y) then
          fBuffer.Draw(DrawX, DrawY, GetFrameRect(IMG_EDIT_TILE), fGraphics);
      end;
    end;

  finally
    fBuffer.EndUpdate;
    CopyBufferToScreen;
  end;
end;

procedure TCubeguyForm.CopyBufferToScreen;
var
  LocalW, LocalH: Integer;
  LocalScrollX, LocalScrollY: Integer;
  ImgScale: Double;
begin
  imgMain.BeginUpdate;
  try
    imgMain.Bitmap.Assign(fBuffer);

    ImgScale := imgMain.Scale;

    LocalW := Round(fBuffer.Width * ImgScale);
    LocalH := Round(fBuffer.Height * ImgScale);

    if fScroll.X < 0 then fScroll.X := 0;
    if fScroll.X >= fState.Width then fScroll.X := fState.Width-1;
    if fScroll.Y < 0 then fScroll.Y := 0;
    if fScroll.Y >= fState.Height then fScroll.Y := fState.Height-1;

    if LocalW <= imgMain.Width then
      imgMain.OffsetHorz := (imgMain.Width - LocalW) div 2
    else begin
      LocalScrollX := fScroll.X * 32 + 16;
      LocalScrollX := Round(LocalScrollX * ImgScale);
      LocalScrollX := LocalScrollX - (imgMain.Width div 2);
      if LocalScrollX < 0 then LocalScrollX := 0;
      if LocalScrollX >= LocalW - imgMain.Width then
        LocalScrollX := LocalW - imgMain.Width - 1;
      imgMain.OffsetHorz := -LocalScrollX;
    end;

    if LocalH <= imgMain.Height then
      imgMain.OffsetVert := (imgMain.Height - LocalH) div 2
    else begin
      LocalScrollY := fScroll.Y * 32 + 16;
      LocalScrollY := Round(LocalScrollY * ImgScale);
      LocalScrollY := LocalScrollY - (imgMain.Height div 2);
      if LocalScrollY < 0 then LocalScrollY := 0;
      if LocalScrollY >= LocalH - imgMain.Height then
        LocalScrollY := LocalH - imgMain.Height - 1;
      imgMain.OffsetVert := -LocalScrollY;
    end;
  finally
    imgMain.EndUpdate;
    imgMain.Changed;
  end;
end;

function TCubeguyForm.GetFrameRect(const ImgIndex: Integer): TRect;
var
  OptOffset: Integer;
begin
  if mi3DView.Checked then
    OptOffset := 0
  else
    OptOffset := SPRITE_HEIGHT;
  Result := Rect(ImgIndex * SPRITE_WIDTH, OptOffset, (ImgIndex + 1) * SPRITE_WIDTH, SPRITE_HEIGHT + OptOffset);
end;

// Audio

procedure TCubeguyForm.PlaySound(const SndIndex: Integer);
begin
  // dummied for now
end;

// Edit mode stuff

function TCubeguyForm.GetMouseTile: TPoint;
begin
  Result := imgMain.ControlToBitmap(imgMain.ScreenToControl(Mouse.CursorPos));
  Result.X := Result.X div 32;
  Result.Y := Result.Y div 32;
end;

procedure TCubeguyForm.HandleEditModeKey(Key: Word; Shift: TShiftState);
var
  EditTile: TPoint;
  x, y: Integer;
begin
  EditTile := GetMouseTile;

  if (ssCtrl in Shift) or (ssMeta in Shift) then
  begin
    case Key of
      VK_LEFT, VK_NUMPAD4: begin
                             fLevel.SetSize(fLevel.Width + 1, fLevel.Height);
                             for y := 0 to fLevel.Height-1 do
                             begin
                               for x := fLevel.Width-2 downto 0 do
                                 fLevel[x+1, y] := fLevel[x, y];
                               fLevel[0, y] := ctEmpty;
                             end;

                             fLevel.Start := Point(fLevel.Start.X + 1, fLevel.Start.Y);
                             fLevel.Goal := Point(fLevel.Goal.X + 1, fLevel.Goal.Y);
                           end;
      VK_UP, VK_NUMPAD8: begin
                           fLevel.SetSize(fLevel.Width, fLevel.Height + 1);
                           for x := 0 to fLevel.Width-1 do
                           begin
                             for y := fLevel.Height-2 downto 0 do
                               fLevel[x, y+1] := fLevel[x, y];
                             fLevel[x, 0] := ctEmpty;
                           end;

                           fLevel.Start := Point(fLevel.Start.X, fLevel.Start.Y + 1);
                           fLevel.Goal := Point(fLevel.Goal.X, fLevel.Goal.Y + 1);
                         end;
      VK_RIGHT, VK_NUMPAD6: fLevel.SetSize(fLevel.Width + 1, fLevel.Height);
      VK_DOWN, VK_NUMPAD2: fLevel.SetSize(fLevel.Width, fLevel.Height + 1);
    end;
  end else if (ssAlt in Shift) then
  begin
    case Key of
      VK_LEFT, VK_NUMPAD4: begin
                             for y := 0 to fLevel.Height-1 do
                               for x := 0 to fLevel.Width-2 do
                                 fLevel[x, y] := fLevel[x+1, y];
                             fLevel.SetSize(fLevel.Width - 1, fLevel.Height);

                             fLevel.Start := Point(fLevel.Start.X - 1, fLevel.Start.Y);
                             fLevel.Goal := Point(fLevel.Goal.X - 1, fLevel.Goal.Y);
                           end;
      VK_UP, VK_NUMPAD8: begin
                           for x := 0 to fLevel.Width-1 do
                             for y := 0 to fLevel.Height-2 do
                               fLevel[x, y] := fLevel[x, y+1];
                           fLevel.SetSize(fLevel.Width, fLevel.Height - 1);

                           fLevel.Start := Point(fLevel.Start.X, fLevel.Start.Y - 1);
                           fLevel.Goal := Point(fLevel.Goal.X, fLevel.Goal.Y - 1);
                         end;
      VK_RIGHT, VK_NUMPAD6: fLevel.SetSize(fLevel.Width - 1, fLevel.Height);
      VK_DOWN, VK_NUMPAD2: fLevel.SetSize(fLevel.Width, fLevel.Height - 1);
    end;
  end else
    case Key of
      VK_LEFT, VK_NUMPAD4: fScroll.X := fScroll.X - 1;
      VK_UP, VK_NUMPAD8: fScroll.Y := fScroll.Y - 1;
      VK_RIGHT, VK_NUMPAD6: fScroll.X := fScroll.X + 1;
      VK_DOWN, VK_NUMPAD2: fScroll.Y := fScroll.Y + 1;
      VK_Z: fLevel[EditTile.X, EditTile.Y] := ctEmpty;
      VK_X: fLevel[EditTile.X, EditTile.Y] := ctBrick;
      VK_C: fLevel[EditTile.X, EditTile.Y] := ctBlock;
      VK_V: fLevel.Goal := EditTile;
      VK_B: begin fLevel.Start := EditTile; fLevel.StartDirection := cdLeft; end;
      VK_N: begin fLevel.Start := EditTile; fLevel.StartDirection := cdRight; end;
    end;

  fState.Assign(fLevel);
  fNeedRender := true;
  fLevelSaved := false;
end;

function TCubeguyForm.CheckSave: Boolean;
begin
  if fLevelSaved then
    Result := true
  else
    case MessageDlg('Save level?', 'Do you want to save the current level?', mtCustom, [mbYes, mbNo, mbCancel], 0) of
      mrYes: Result := DoSaveLevel;
      mrNo: Result := true;
      mrCancel: Result := false;
    end;
end;

end.

