unit CubeguyLevel;

interface

uses
  Classes,
  SysUtils;

type
  TCubeguyDirection = (cdLeft, cdRight);
  TCubeguyTile = (ctEmpty, ctBrick, ctBlock, ctBlockGravity);
  TCubeguyAction = (caNone, caRight, caLeft, caUp, caPick);

  TCubeguyLevelBase = class
    private
      fTilemap: array of array of TCubeguyTile;
      fWidth: Integer;
      fHeight: Integer;
      function GetTile(const X, Y: Integer): TCubeguyTile;
      procedure SetTile(const X, Y: Integer; const aValue: TCubeguyTile);
      procedure SetWidth(const aValue: Integer);
      procedure SetHeight(const aValue: Integer);
      procedure InternalSetSize(aWidth, aHeight: Integer);
    public
      constructor Create;
      procedure SetSize(aWidth, aHeight: Integer);

      procedure Clear; virtual;
      procedure Assign(const aSource: TCubeguyLevelBase); virtual;

      property Tiles[X, Y: Integer]: TCubeguyTile read GetTile write SetTile; default;
      property Width: Integer read fWidth write SetWidth;
      property Height: Integer read fHeight write SetHeight;
  end;

  TCubeguyLevel = class(TCubeguyLevelBase)
    private
      fStart: TPoint;
      fStartDirection: TCubeguyDirection;
      fGoal: TPoint;

      procedure SetStart(const aValue: TPoint);
      procedure SetGoal(const aValue: TPoint);
    public
      procedure Clear; override;
      procedure Assign(const aSource: TCubeguyLevelBase); override;

      procedure LoadFromFile(aFilename: TFilename);
      procedure SaveToFile(aFilename: TFilename);
      procedure LoadFromStream(aStream: TStream);
      procedure SaveToStream(aStream: TStream);

      property Start: TPoint read fStart write SetStart;
      property StartDirection: TCubeguyDirection read fStartDirection write fStartDirection;
      property Goal: TPoint read fGoal write SetGoal;
  end;

  TCubeguyLevelState = class(TCubeguyLevelBase)
    private
      fAction: TCubeguyAction;
      fGoal: TPoint;
      fPosition: TPoint;
      fDirection: TCubeguyDirection;
      fHoldingBlock: Boolean;

      function GetDX: Integer;
    public
      procedure Clear; override;
      procedure Assign(const aSource: TCubeguyLevelBase); override;

      property Action: TCubeguyAction read fAction write fAction;
      property Goal: TPoint read fGoal write fGoal;
      property Position: TPoint read fPosition write fPosition;
      property Direction: TCubeguyDirection read fDirection write fDirection;
      property HoldingBlock: Boolean read fHoldingBlock write fHoldingBlock;
      property DX: Integer read GetDX;
  end;

implementation

// TCubeguyLevelState

procedure TCubeguyLevelState.Clear;
begin
  inherited;
  fGoal := Point(0, 0);
  fPosition := Point(0, 0);
  fDirection := cdLeft;
  fHoldingBlock := false;
  fAction := caNone;
end;

procedure TCubeguyLevelState.Assign(const aSource: TCubeguyLevelBase);
var
  SrcLvl: TCubeguyLevel absolute aSource;
  SrcState: TCubeguyLevelState absolute aSource;
begin
  inherited;

  if aSource is TCubeguyLevel then
  begin
    fGoal := SrcLvl.fGoal;
    fPosition := SrcLvl.fStart;
    fDirection := SrcLvl.fStartDirection;
    fHoldingBlock := false;
    fAction := caNone;
  end;

  if aSource is TCubeguyLevelState then
  begin
    fGoal := SrcState.fGoal;
    fPosition := SrcState.fPosition;
    fDirection := SrcState.fDirection;
    fHoldingBlock := SrcState.fHoldingBlock;
    fAction := SrcState.fAction;
  end;
end;

function TCubeguyLevelState.GetDX: Integer;
begin
  if fDirection = cdLeft then
    Result := -1
  else
    Result := 1;
end;

// TCubeguyLevel

constructor TCubeguyLevelBase.Create;
begin
  inherited;
  Clear;
end;

procedure TCubeguyLevelBase.SetSize(aWidth, aHeight: Integer);
begin
  if (aWidth < 1) then aWidth := 1;
  if (aHeight < 1) then aHeight := 1;
  if (aWidth = fWidth) and (aHeight = fHeight) then Exit;
  InternalSetSize(aWidth, aHeight);
end;

procedure TCubeguyLevelBase.InternalSetSize(aWidth, aHeight: Integer);
var
  x: Integer;
begin
  SetLength(fTilemap, aWidth);
  for x := 0 to aWidth-1 do
    SetLength(fTileMap[x], aHeight);
  fWidth := aWidth;
  fHeight := aHeight;
end;

procedure TCubeguyLevelBase.SetWidth(const aValue: Integer);
begin
  SetSize(aValue, fHeight);
end;

procedure TCubeguyLevelBase.SetHeight(const aValue: Integer);
begin
  SetSize(fWidth, aValue);
end;

procedure TCubeguyLevelBase.SetTile(const X, Y: Integer; const aValue: TCubeguyTile);
begin
  if (X < 0) or (X >= fWidth) or
     (Y < 0) or (Y >= fHeight) then
    Exit;
  fTilemap[X][Y] := aValue;
end;

function TCubeguyLevelBase.GetTile(const X, Y: Integer): TCubeguyTile;
begin
  if (X < 0) or (X >= fWidth) or
     (Y < 0) or (Y >= fHeight) then
    Result := ctBrick
  else
    Result := fTilemap[X][Y];
end;

procedure TCubeguyLevelBase.Clear;
begin
  SetSize(1, 1);
  fTilemap[0][0] := ctEmpty;
end;

procedure TCubeguyLevelBase.Assign(const aSource: TCubeguyLevelBase);
var
  x, y: Integer;
begin
  fWidth := aSource.fWidth;
  fHeight := aSource.fHeight;

  SetLength(fTilemap, fWidth);
  for x := 0 to fWidth-1 do
  begin
    SetLength(fTilemap[x], fHeight);
    for y := 0 to fHeight-1 do
      fTilemap[x][y] := aSource.fTilemap[x][y];
  end;
end;

// TCubeguyLevel

procedure TCubeguyLevel.Clear;
begin
  inherited;
  fStart := Point(0, 0);
  fGoal := Point(0, 0);
  fStartDirection := cdRight;
end;

procedure TCubeguyLevel.Assign(const aSource: TCubeguyLevelBase);
var
  SrcLocal: TCubeguyLevel absolute aSource;
begin
  inherited;
  if not (aSource is TCubeguyLevel) then Exit;
  fStart := SrcLocal.fStart;
  fStartDirection := SrcLocal.fStartDirection;
  fGoal := SrcLocal.fGoal;
end;

procedure TCubeguyLevel.SetStart(const aValue: TPoint);
begin
  if (aValue.X < 0) or (aValue.X >= fWidth) or
     (aValue.Y < 0) or (aValue.Y >= fHeight) then
    Exit;
  fStart := aValue;
end;

procedure TCubeguyLevel.SetGoal(const aValue: TPoint);
begin
  if (aValue.X < 0) or (aValue.X >= fWidth) or
     (aValue.Y < 0) or (aValue.Y >= fHeight) then
    Exit;
  fGoal := aValue;
end;

procedure TCubeguyLevel.LoadFromFile(aFilename: TFilename);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFilename, fmOpenRead);
  try
    LoadFromStream(F);
  finally
    F.Free;
  end;
end;

procedure TCubeguyLevel.SaveToFile(aFilename: TFilename);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFilename, fmCreate);
  try
    SaveToStream(F);
  finally
    F.Free;
  end;
end;

procedure TCubeguyLevel.LoadFromStream(aStream: TStream);
var
  x, y: Integer;
begin
  Clear;

  aStream.Read(fWidth, 4);
  aStream.Read(fHeight, 4);
  InternalSetSize(fWidth, fHeight);

  aStream.Read(fStart.X, 4);
  aStream.Read(fStart.Y, 4);
  aStream.Read(fGoal.X, 4);
  aStream.Read(fGoal.Y, 4);
  aStream.Read(fStartDirection, 1);

  for y := 0 to fHeight-1 do
    for x := 0 to fWidth-1 do
      aStream.Read(fTilemap[x][y], 1);
end;

procedure TCubeguyLevel.SaveToStream(aStream: TStream);
var
  x, y: Integer;
begin
  aStream.Write(fWidth, 4);
  aStream.Write(fHeight, 4);

  aStream.Write(fStart.X, 4);
  aStream.Write(fStart.Y, 4);
  aStream.Write(fGoal.X, 4);
  aStream.Write(fGoal.Y, 4);
  aStream.Write(fStartDirection, 1);

  for y := 0 to fHeight-1 do
    for x := 0 to fWidth-1 do
      aStream.Write(fTilemap[x][y], 1);
end;

end.
