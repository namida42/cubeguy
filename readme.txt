CubeGuy
by Namida Verasche

---

LICENCE: All code and graphics for CubeGuy are hereby released into the public domain.
         Levels and replay files remain the property of their creators.

---

BUILD INSTRUCTIONS:
1. Install Lazarus IDE (https://www.lazarus-ide.org/)
2. Install the Online Package Manager for Lazarus (in Lazarus: Package -> Install / Uninstall Packages -> Online Package Manager)
3. Using the Online Package Manager, install the Graphics32 package
4. Open and compile Cubeguy.lpi

Confirmed working with Windows + Lazarus 1.8.0 64-bit + FPC 3.0.4 64-bit.

Should theoretically work on any fairly close (or newer) version of Lazarus / FPC.
Should theoretically work on 32-bit.
Should theoretically work on Mac and Linux.
But these are all untested.
